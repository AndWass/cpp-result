
//          Copyright Andreas Wass 2019.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE or copy at
//          https://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <filesystem>

#include "andwass/result.hpp"

namespace andwass
{
namespace fs
{
namespace stdfs = std::filesystem;

#define RESULT(X) inline andwass::result<X, std::error_code>

#define RETVOID()                                                                                  \
    if (ec) {                                                                                      \
        return andwass::error(std::move(ec));                                                      \
    }                                                                                              \
    return andwass::ok()

#define RETVAL(R)                                                                                  \
    if (ec) {                                                                                      \
        return andwass::error(std::move(ec));                                                      \
    }                                                                                              \
    return andwass::ok(std::move(R))

#define FS_FUN(RET, FUN)                                                                           \
    inline andwass::result<RET, std::error_code> FUN(const stdfs::path& p) {                       \
        std::error_code ec;                                                                        \
        auto retval = stdfs::FUN(p, ec);                                                           \
        if (ec) {                                                                                  \
            return andwass::error(std::move(ec));                                                  \
        }                                                                                          \
        return andwass::ok(std::move(retval));                                                     \
    }

FS_FUN(stdfs::path, absolute)
FS_FUN(stdfs::path, canonical)
FS_FUN(stdfs::path, weakly_canonical)
FS_FUN(stdfs::path, relative)
FS_FUN(stdfs::path, proximate)

RESULT(stdfs::path)
relative(const stdfs::path& p, const stdfs::path& base = stdfs::current_path()) {
    std::error_code ec;
    auto retval = stdfs::relative(p, base, ec);
    RETVAL(retval);
}
RESULT(stdfs::path)
proximate(const stdfs::path& p, const stdfs::path& base = stdfs::current_path()) {
    std::error_code ec;
    auto retval = stdfs::proximate(p, base, ec);
    RETVAL(retval);
}

RESULT(void) copy(const stdfs::path & from, const stdfs::path & to) {
    std::error_code ec;
    stdfs::copy(from, to, ec);
    RETVOID();
}

RESULT(void)
copy(const stdfs::path& from, const stdfs::path& to, stdfs::copy_options options) {
    std::error_code ec;
    stdfs::copy(from, to, options, ec);
    RETVOID();
}

RESULT(bool) copy_file(const stdfs::path & from, const stdfs::path & to) {
    std::error_code ec;
    auto retval = stdfs::copy_file(from, to, ec);
    RETVAL(retval);
}

RESULT(bool)
copy_file(const stdfs::path& from, const stdfs::path& to, stdfs::copy_options options) {
    std::error_code ec;
    auto retval = stdfs::copy_file(from, to, options, ec);
    RETVAL(retval);
}

RESULT(void) copy_symlink(const stdfs::path & from, const stdfs::path & to) {
    std::error_code ec;
    stdfs::copy_symlink(from, to, ec);
    RETVOID();
}

FS_FUN(bool, create_directory)
RESULT(bool) create_directory(const stdfs::path & p, const stdfs::path & existing_p) {
    std::error_code ec;
    auto retval = stdfs::create_directory(p, existing_p, ec);
    RETVAL(retval);
}

FS_FUN(bool, create_directories)

RESULT(void)
create_hard_link(const std::filesystem::path& target,
                 const std::filesystem::path& link) noexcept {
    std::error_code ec;
    stdfs::create_hard_link(target, link, ec);
    RETVOID();
}

RESULT(void)
create_symlink(const stdfs::path& target, const stdfs::path& link) noexcept {
    std::error_code ec;
    stdfs::create_symlink(target, link, ec);
    RETVOID();
}

RESULT(void)
create_directory_symlink(const stdfs::path& target, const stdfs::path& link) noexcept {
    std::error_code ec;
    stdfs::create_directory_symlink(target, link, ec);
    RETVOID();
}

RESULT(stdfs::path) current_path() {
    std::error_code ec;
    auto retval = stdfs::current_path(ec);
    RETVAL(retval);
}

FS_FUN(bool, exists)

RESULT(bool) equivalent(const stdfs::path & p1, const stdfs::path & p2) {
    std::error_code ec;
    auto retval = stdfs::equivalent(p1, p2);
    RETVAL(retval);
}

FS_FUN(std::uintmax_t, file_size)
FS_FUN(std::uintmax_t, hard_link_count)
FS_FUN(stdfs::file_time_type, last_write_time)

RESULT(void)
last_write_time(const std::filesystem::path& p, std::filesystem::file_time_type new_time) {
    std::error_code ec;
    stdfs::last_write_time(p, new_time, ec);
    RETVOID();
}

RESULT(void)
permissions(const std::filesystem::path& p,
            std::filesystem::perms prms,
            std::filesystem::perm_options opts = stdfs::perm_options::replace) {
    std::error_code ec;
    stdfs::permissions(p, prms, opts, ec);
    RETVOID();
}

FS_FUN(stdfs::path, read_symlink)
FS_FUN(bool, remove)
FS_FUN(std::uintmax_t, remove_all)

RESULT(void) rename(const std::filesystem::path & old_p, const std::filesystem::path & new_p) {
    std::error_code ec;
    stdfs::rename(old_p, new_p, ec);
    RETVOID();
}

RESULT(void) resize_file(const std::filesystem::path & p, std::uintmax_t new_size) {
    std::error_code ec;
    stdfs::resize_file(p, new_size, ec);
    RETVOID();
}

FS_FUN(stdfs::space_info, space)
FS_FUN(stdfs::file_status, status)
FS_FUN(stdfs::file_status, symlink_status)

RESULT(stdfs::path) temp_directory_path() {
    std::error_code ec;
    auto retval = stdfs::temp_directory_path(ec);
    RETVAL(retval);
}

FS_FUN(bool, is_block_file)
FS_FUN(bool, is_character_file)
FS_FUN(bool, is_directory)
FS_FUN(bool, is_empty)
FS_FUN(bool, is_fifo)
FS_FUN(bool, is_other)
FS_FUN(bool, is_regular_file)
FS_FUN(bool, is_socket)
FS_FUN(bool, is_symlink)

} // namespace fs
} // namespace andwass