
//          Copyright Andreas Wass 2019.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE or copy at
//          https://www.boost.org/LICENSE_1_0.txt)

#include "filesystem.hpp"
#include <iostream>

namespace fs = std::filesystem;

andwass::result<fs::path, std::error_code> get_path() {
    fs::path p;
    std::cout << "Enter a path: ";
    std::cin >> p;
    if(std::cin) {
        return andwass::ok(p);
    }
    return andwass::error(std::make_error_code(std::io_errc::stream));
}

void result_based() {
    auto e = get_path().and_then([](auto&& path) {
        std::cout << path.string() << std::endl;
        return andwass::fs::file_size(path);
    }).and_then([](std::size_t size) {
        std::cout << "Size = " << size << std::endl;
        return andwass::ok(size);
    }).or_else([](auto&& err) {
        std::cout << "Error = " << err.message() << std::endl;
        return andwass::error(std::move(err));
    });

    std::cout << e.is_error() << std::endl;
}

void ec_based() {
    std::error_code ec;
    fs::path path;
    std::cout << "Enter path: ";
    std::cin >> path;
    if(!std::cin) {
        ec = std::make_error_code(std::io_errc::stream);
    }
    else {
        std::cout << path << std::endl;
        auto size = fs::file_size(path, ec);
        if(!ec) {
            std::cout << "Size = " << size << std::endl;
        }
    }

    if(ec) {
        std::cout << "Error = " << ec.message() << std::endl;
        std::cout << static_cast<bool>(ec) << std::endl;
    }
}

int main() {
    result_based();
    //ec_based();
}
