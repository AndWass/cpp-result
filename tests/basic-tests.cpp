#include "andwass/result.hpp"
#include "catch.hpp"

#include <atomic>
#include <memory>
#include <string>

template class andwass::result<void, int>;

andwass::result<std::unique_ptr<int>, char *> get_up_res() {
    return andwass::ok(std::make_unique<int>(254));
}

struct tracer
{
    tracer() noexcept {
        default_construct_count++;
    }
    tracer(const tracer &) noexcept {
        copy_construct_count++;
    }
    tracer(tracer &&) noexcept {
        move_construct_count++;
    }

    tracer &operator=(const tracer &) noexcept {
        copy_assign_count++;
        return *this;
    }

    tracer &operator=(tracer &&) noexcept {
        move_assign_count++;
        return *this;
    }

    ~tracer() noexcept {
        destruct_count++;
    }

    static void reset() {
        copy_construct_count = 0;
        move_construct_count = 0;
        default_construct_count = 0;
        move_assign_count = 0;
        copy_assign_count = 0;
        destruct_count = 0;
    }
    static int copy_construct_count;
    static int move_construct_count;
    static int default_construct_count;
    static int move_assign_count;
    static int copy_assign_count;
    static int destruct_count;
};

template class andwass::result<tracer, int>;

struct nontrivial_destruct
{
    ~nontrivial_destruct() {
    }
};

struct trivial_copy_move
{
};

struct trivial_copy_nontrivial_move
{
    trivial_copy_nontrivial_move(const trivial_copy_nontrivial_move &) = default;
    trivial_copy_nontrivial_move(trivial_copy_nontrivial_move &&) {
    }
};

struct nontrivial_copy_nontrivial_move
{
    nontrivial_copy_nontrivial_move(const nontrivial_copy_nontrivial_move &) {
    }
    nontrivial_copy_nontrivial_move(nontrivial_copy_nontrivial_move &&) {
    }
};

struct nontrivial_copy_trivial_move
{
    nontrivial_copy_trivial_move() = default;
    nontrivial_copy_trivial_move(const nontrivial_copy_trivial_move &) {
    }
    nontrivial_copy_trivial_move(nontrivial_copy_trivial_move &&) = default;

    nontrivial_copy_trivial_move &operator=(const nontrivial_copy_trivial_move &) {
        return *this;
    }
    nontrivial_copy_trivial_move &operator=(nontrivial_copy_trivial_move &&) = default;
};

int tracer::copy_construct_count = 0;
int tracer::move_construct_count = 0;
int tracer::default_construct_count = 0;
int tracer::move_assign_count = 0;
int tracer::copy_assign_count = 0;
int tracer::destruct_count = 0;

TEST_CASE("basic-tests: default-constructed ok", "[basic-tests]") {
    {
        andwass::result<void, int> default_constructed;
        REQUIRE(default_constructed.is_ok());
        REQUIRE(static_cast<bool>(default_constructed));
    }

    {
        andwass::result<int, int> default_constructed;
        REQUIRE(default_constructed.is_ok());
        REQUIRE(static_cast<bool>(default_constructed));
    }

    {
        struct temp
        {
            temp() {
            }
        };
        andwass::result<temp, int> default_constructed;
        REQUIRE(default_constructed.is_ok());
        REQUIRE(static_cast<bool>(default_constructed));
    }
    {
        class temp
        {
            temp() = delete;
        };
        // Compilation test only, should fail
        //andwass::result<temp, int> default_constructed;
        // REQUIRE(default_constructed.is_ok());
    }
}

TEST_CASE("basic-tests: ok-constructed", "[basic-tests]") {
    {
        andwass::result<void, int> res{andwass::ok()};
        REQUIRE(res.is_ok());
        REQUIRE(static_cast<bool>(res));
        if(res) { // Compilation test...
            REQUIRE(true);
        }
    }

    {
        auto ok = andwass::ok();
        andwass::result<void, int> res{ok};
        REQUIRE(res.is_ok());
        REQUIRE(static_cast<bool>(res));
    }

    {
        auto ok = andwass::ok(1);
        andwass::result<int, int> res{ok};
        REQUIRE(res.is_ok());
        REQUIRE(static_cast<bool>(res));
    }

    {
        auto ok = andwass::ok(trivial_copy_move{});
        andwass::result<trivial_copy_move, int> res{ok};
        REQUIRE(res.is_ok());
        REQUIRE(static_cast<bool>(res));
    }
}

TEST_CASE("basic-tests: error-constructed", "[basic-tests]") {
    {
        andwass::result<void, int> res{andwass::error(-1)};
        REQUIRE(res.is_error());
        REQUIRE_FALSE(static_cast<bool>(res));
    }

    {
        auto err = andwass::error(-1);
        andwass::result<void, int> res{err};
        REQUIRE(res.is_error());
        REQUIRE_FALSE(static_cast<bool>(res));
    }

    {
        const auto err = andwass::error(1);
        andwass::result<int, int> res{err};
        REQUIRE(res.is_error());
        REQUIRE_FALSE(static_cast<bool>(res));
    }
}

TEST_CASE("basic-tests: copy-constructed", "[basic-tests]") {
    {
        andwass::result<void, int> default_constructed;
        auto copy = default_constructed;
        REQUIRE(copy.is_ok());
    }

    {
        andwass::result<int, int> error{andwass::error(10)};
        REQUIRE_FALSE(error.is_ok());
        auto copy = error;
        REQUIRE_FALSE(copy.is_ok());
    }
}

TEST_CASE("basic-tests: move-constructed", "[basic-tests]") {
    {
        andwass::result<void, int> default_constructed;
        auto copy = std::move(default_constructed);
        REQUIRE(copy.is_ok());
    }

    {
        andwass::result<int, int> error{andwass::error(10)};
        REQUIRE_FALSE(error.is_ok());
        auto copy = std::move(error);
        REQUIRE_FALSE(copy.is_ok());
    }
}

TEST_CASE("basic-tests: copy-assigned", "[basic-tests]") {
    {
        andwass::result<trivial_copy_move, int> default_constructed;
        andwass::result<trivial_copy_move, int> copy;
        copy = default_constructed;
        REQUIRE(copy.is_ok());
    }

    {
        andwass::result<nontrivial_copy_trivial_move, int> default_constructed;
        andwass::result<nontrivial_copy_trivial_move, int> copy;
        copy = default_constructed;
        REQUIRE(copy.is_ok());
    }
}

TEST_CASE("basic-tests: trace default-construction", "[basic-tests]") {
    tracer::reset();
    andwass::result<tracer, int> dflt;
    REQUIRE(tracer::default_construct_count == 1);
    REQUIRE(tracer::copy_construct_count == 0);
    REQUIRE(tracer::move_construct_count == 0);
    REQUIRE(tracer::copy_assign_count == 0);
    REQUIRE(tracer::move_assign_count == 0);

    andwass::result<tracer, int> dflt3;
    tracer::reset();
    auto dflt2 = dflt;
    REQUIRE(tracer::copy_construct_count == 1);
    auto dflt4{std::move(dflt)};
    REQUIRE(tracer::move_construct_count == 1);

    REQUIRE(tracer::copy_assign_count == 0);
    REQUIRE(tracer::move_assign_count == 0);

    tracer::reset();
    dflt2 = dflt3;
    REQUIRE(tracer::copy_assign_count == 1);
    REQUIRE(tracer::destruct_count == 0);
    dflt = std::move(dflt2);
    REQUIRE(tracer::move_assign_count == 1);
    REQUIRE(tracer::destruct_count == 0);

    dflt2 = dflt;
    dflt3 = dflt2;

    { andwass::result<tracer, int> dflt6; }
    REQUIRE(tracer::destruct_count == 1);

    tracer::reset();
    { andwass::result<tracer, int> err(andwass::error(0)); }
    REQUIRE(tracer::default_construct_count == 0);
    REQUIRE(tracer::copy_construct_count == 0);
    REQUIRE(tracer::move_construct_count == 0);
    REQUIRE(tracer::copy_assign_count == 0);
    REQUIRE(tracer::move_assign_count == 0);
}

TEST_CASE("basic-tests: triviality propogation", "[basic-tests]") {
    REQUIRE(std::is_trivially_destructible<andwass::result<int, float>>::value);

    REQUIRE(!std::is_trivially_destructible<andwass::result<nontrivial_destruct, int>>::value);
    REQUIRE(!std::is_trivially_destructible<andwass::result<int, nontrivial_destruct>>::value);
    REQUIRE(!std::is_trivially_destructible<
            andwass::result<nontrivial_destruct, nontrivial_destruct>>::value);

    REQUIRE(std::is_trivially_destructible<
            andwass::result<trivial_copy_move, trivial_copy_move>>::value);

    REQUIRE(std::is_trivially_copy_constructible<andwass::result<trivial_copy_move, int>>::value);
    REQUIRE(std::is_trivially_move_constructible<andwass::result<trivial_copy_move, int>>::value);

    REQUIRE(std::is_trivially_copy_constructible<
            andwass::result<trivial_copy_nontrivial_move, int>>::value);
    REQUIRE(!std::is_trivially_move_constructible<
            andwass::result<trivial_copy_nontrivial_move, int>>::value);
    REQUIRE(!std::is_trivially_move_constructible<
            andwass::result<int, trivial_copy_nontrivial_move>>::value);

    REQUIRE(!std::is_trivially_copy_constructible<
            andwass::result<nontrivial_copy_trivial_move, int>>::value);
    REQUIRE(std::is_trivially_move_constructible<
            andwass::result<nontrivial_copy_trivial_move, int>>::value);
    REQUIRE(!std::is_trivially_copy_constructible<
            andwass::result<nontrivial_copy_nontrivial_move, int>>::value);
    REQUIRE(!std::is_trivially_move_constructible<
            andwass::result<nontrivial_copy_nontrivial_move, int>>::value);
}

TEST_CASE("basic-tests: ok_value", "[basic-tests]") {
    {
        andwass::result<void, int> res;
        auto val = res.maybe_value();
        REQUIRE(res.is_ok());
        REQUIRE(val.has_value());
    }
    {
        andwass::result<int, int> res{andwass::ok(1)};
        auto val = res.maybe_value();
        REQUIRE(res.is_ok());
        REQUIRE(val.has_value());
        REQUIRE(val.value() == 1);
    }
    {
        andwass::result<std::string, int> res{andwass::ok(std::string("hello"))};
        auto val = res.maybe_value();
        REQUIRE(res.is_ok());
        REQUIRE(val.has_value());
        REQUIRE(val.value() == "hello");
    }

    {
        const andwass::result<std::string, int> res{andwass::ok(std::string("hello"))};
        auto val = res.maybe_value();
        REQUIRE(res.is_ok());
        REQUIRE(val.has_value());
        REQUIRE(val.value() == "hello");
    }

    {
        andwass::result<std::string, int> res{andwass::ok(std::string("hello"))};
        auto val = std::move(res).maybe_value();
        REQUIRE(val.has_value());
        REQUIRE(val.value() == "hello");
    }

    {
        const andwass::result<std::string, int> res{andwass::ok(std::string("hello"))};
        auto val = std::move(res).maybe_value();
        REQUIRE(res.is_ok());
        REQUIRE(val.has_value());
        REQUIRE(val.value() == "hello");
    }

    {
        andwass::result<void, int> res{andwass::error(-1)};
        auto val = res.maybe_value();
        REQUIRE_FALSE(res.is_ok());
        REQUIRE_FALSE(val.has_value());
    }
    {
        andwass::result<int, int> res{andwass::error(1)};
        auto val = res.maybe_value();
        REQUIRE_FALSE(res.is_ok());
        REQUIRE_FALSE(val.has_value());
    }
    {
        andwass::result<int, std::string> res{andwass::error(std::string("hello"))};
        auto val = res.maybe_value();
        REQUIRE_FALSE(res.is_ok());
        REQUIRE_FALSE(val.has_value());
    }

    {
        const andwass::result<int, std::string> res{andwass::error(std::string("hello"))};
        auto val = res.maybe_value();
        REQUIRE_FALSE(res.is_ok());
        REQUIRE_FALSE(val.has_value());
    }

    {
        andwass::result<int, std::string> res{andwass::error(std::string("hello"))};
        auto val = std::move(res).maybe_value();
        REQUIRE_FALSE(res.is_ok());
        REQUIRE_FALSE(val.has_value());
    }

    {
        const andwass::result<int, std::string> res{andwass::error(std::string("hello"))};
        auto val = std::move(res).maybe_value();
        REQUIRE_FALSE(res.is_ok());
        REQUIRE_FALSE(val.has_value());
    }
}

TEST_CASE("basic-tests: error_value", "[basic-tests]") {
    {
        andwass::result<void, int> res;
        auto val = res.maybe_error();
        REQUIRE_FALSE(res.is_error());
        REQUIRE_FALSE(val.has_value());
    }
    {
        andwass::result<int, int> res{andwass::ok(1)};
        auto val = res.maybe_error();
        REQUIRE_FALSE(res.is_error());
        REQUIRE_FALSE(val.has_value());
    }
    {
        andwass::result<std::string, int> res{andwass::ok(std::string("hello"))};
        auto val = res.maybe_error();
        REQUIRE_FALSE(res.is_error());
        REQUIRE_FALSE(val.has_value());
    }

    {
        const andwass::result<std::string, int> res{andwass::ok(std::string("hello"))};
        auto val = res.maybe_error();
        REQUIRE_FALSE(res.is_error());
        REQUIRE_FALSE(val.has_value());
    }

    {
        andwass::result<std::string, int> res{andwass::ok(std::string("hello"))};
        auto val = std::move(res).maybe_error();
        REQUIRE_FALSE(val.has_value());
    }

    {
        const andwass::result<std::string, int> res{andwass::ok(std::string("hello"))};
        auto val = std::move(res).maybe_error();
        REQUIRE_FALSE(res.is_error());
        REQUIRE_FALSE(val.has_value());
    }

    {
        andwass::result<void, int> res{andwass::error(-1)};
        auto val = res.maybe_error();
        REQUIRE(res.is_error());
        REQUIRE(val.has_value());
        REQUIRE(val.value() == -1);
    }
    {
        andwass::result<int, int> res{andwass::error(1)};
        auto val = res.maybe_error();
        REQUIRE(res.is_error());
        REQUIRE(val.has_value());
        REQUIRE(val.value() == 1);
    }
    {
        andwass::result<int, std::string> res{andwass::error(std::string("hello"))};
        auto val = res.maybe_error();
        REQUIRE(res.is_error());
        REQUIRE(val.has_value());
        REQUIRE(val.value() == "hello");
    }
    {
        const andwass::result<int, std::string> res{andwass::error(std::string("hello"))};
        auto val = res.maybe_error();
        REQUIRE(res.is_error());
        REQUIRE(val.has_value());
        REQUIRE(val.value() == "hello");
    }
    {
        andwass::result<int, std::string> res{andwass::error(std::string("hello"))};
        auto val = std::move(res).maybe_error();
        REQUIRE(val.has_value());
        REQUIRE(val.value() == "hello");
    }
    {
        const andwass::result<int, std::string> res{andwass::error(std::string("hello"))};
        auto val = std::move(res).maybe_error();
        REQUIRE(res.is_error());
        REQUIRE(val.has_value());
        REQUIRE(val.value() == "hello");
    }
}

TEST_CASE("basic-tests: map", "[basic-tests]") {
    {
        andwass::result<int, int> res = andwass::ok(1234);
        auto mapped = res.map([](int i) -> int { return i; });
        REQUIRE(res.is_ok());
        REQUIRE(mapped.is_ok());
        REQUIRE(*mapped.maybe_value() == 1234);
    }
    {
        const andwass::result<int, int> res = andwass::ok(1234);
        auto mapped = res.map([](auto i) { return std::to_string(i); });
        REQUIRE(res.is_ok());
        REQUIRE(mapped.is_ok());
        REQUIRE(*mapped.maybe_value() == "1234");
    }
    {
        andwass::result<int, int> res = andwass::ok(1234);
        auto mapped = std::move(res).map([](auto i) { return std::to_string(i); });
        REQUIRE(mapped.is_ok());
        REQUIRE(*mapped.maybe_value() == "1234");
    }

    {
        const andwass::result<int, int> res = andwass::ok(1234);
        auto mapped = std::move(res).map([](auto i) { return std::to_string(i); });
        REQUIRE(res.is_ok());
        REQUIRE(mapped.is_ok());
        REQUIRE(*mapped.maybe_value() == "1234");
    }
    {
        andwass::result<int, int> res = andwass::error(1234);
        auto mapped = res.map([](auto i) { return std::to_string(i); });
        REQUIRE(res.is_error());
        REQUIRE(mapped.is_error());
        REQUIRE(*mapped.maybe_error() == 1234);
    }

    {
        const andwass::result<int, int> res = andwass::ok(1234);
        auto mapped = std::move(res).map([](auto i) {});
        REQUIRE(res.is_ok());
        REQUIRE(mapped.is_ok());
    }
    {
        andwass::result<int, int> res = andwass::error(1234);
        auto mapped = res.map([](auto i) {});
        REQUIRE(res.is_error());
        REQUIRE(mapped.is_error());
        REQUIRE(*mapped.maybe_error() == 1234);
    }

    {
        const andwass::result<void, int> res = andwass::ok();
        auto mapped = std::move(res).map([]() { return 1234; });
        REQUIRE(res.is_ok());
        REQUIRE(mapped.is_ok());
        REQUIRE(*mapped.maybe_value() == 1234);
    }
    {
        andwass::result<void, int> res = andwass::error(1234);
        auto mapped = res.map([]() { return std::string("ok"); });
        REQUIRE(res.is_error());
        REQUIRE(mapped.is_error());
        REQUIRE(*mapped.maybe_error() == 1234);
    }

    {
        const andwass::result<void, int> res = andwass::ok();
        auto mapped = std::move(res).map([]() {});
        REQUIRE(res.is_ok());
        REQUIRE(mapped.is_ok());
    }
    {
        andwass::result<void, int> res = andwass::error(1234);
        auto mapped = res.map([]() {});
        REQUIRE(res.is_error());
        REQUIRE(mapped.is_error());
        REQUIRE(*mapped.maybe_error() == 1234);
    }
}

TEST_CASE("basic-tests: map_or_else", "[basic-tests]") {
    {
        andwass::result<int, int> res = andwass::ok(1234);
        auto mapped = res.map_or_else([](auto i) { return std::to_string(i); },
                                      [](auto err) { return "error"; });
        REQUIRE(res.is_ok());
        REQUIRE(mapped == "1234");
    }

    {
        const andwass::result<int, int> res = andwass::ok(1234);
        auto mapped = res.map_or_else([](auto i) { return std::to_string(i); },
                                      [](auto err) { return "error"; });
        REQUIRE(res.is_ok());
        REQUIRE(mapped == "1234");
    }

    {
        andwass::result<int, int> res = andwass::ok(1234);
        auto mapped = std::move(res).map_or_else([](auto i) { return std::to_string(i); },
                                                 [](auto err) { return "error"; });
        REQUIRE(mapped == "1234");
    }

    {
        const andwass::result<int, int> res = andwass::ok(1234);
        auto mapped = std::move(res).map_or_else([](auto i) { return std::to_string(i); },
                                                 [](auto err) { return "error"; });
        REQUIRE(res.is_ok());
        REQUIRE(mapped == "1234");
    }

    {
        andwass::result<int, int> res = andwass::error(1234);
        auto mapped = res.map_or_else([](auto i) { return std::to_string(i); },
                                      [](auto err) { return "error"; });
        REQUIRE(res.is_error());
        REQUIRE(mapped == "error");
    }
}

TEST_CASE("basic-tests: map_error", "[basic-tests]") {
    {
        andwass::result<int, int> res = andwass::error(1234);
        auto mapped = res.map_error([](auto i) { return std::to_string(i); });
        REQUIRE(res.is_error());
        REQUIRE(*res.maybe_error() == 1234);
        REQUIRE(mapped.is_error());
        REQUIRE(*mapped.maybe_error() == "1234");
    }

    {
        const andwass::result<int, int> res = andwass::error(1234);
        auto mapped = res.map_error([](auto i) { return std::to_string(i); });
        REQUIRE(res.is_error());
        REQUIRE(*res.maybe_error() == 1234);
        REQUIRE(mapped.is_error());
        REQUIRE(*mapped.maybe_error() == "1234");
    }

    {
        andwass::result<int, int> res = andwass::error(1234);
        auto mapped = std::move(res).map_error([](auto i) { return std::to_string(i); });
        REQUIRE(mapped.is_error());
        REQUIRE(*mapped.maybe_error() == "1234");
    }

    {
        const andwass::result<int, int> res = andwass::error(1234);
        auto mapped = std::move(res).map_error([](auto i) { return std::to_string(i); });
        REQUIRE(res.is_error());
        REQUIRE(*res.maybe_error() == 1234);
        REQUIRE(mapped.is_error());
        REQUIRE(*mapped.maybe_error() == "1234");
    }

    {
        andwass::result<int, int> res = andwass::ok(1234);
        auto mapped = res.map_error([](auto i) { return std::to_string(i); });
        REQUIRE(res.is_ok());
        REQUIRE(*res.maybe_value() == 1234);
        REQUIRE(mapped.is_ok());
        REQUIRE(*mapped.maybe_value() == 1234);
    }
}

TEST_CASE("basic-tests: and_then", "[basic-tests]") {
    {
        andwass::result<int, int> res = andwass::ok(1234);
        auto mapped = res.and_then([](auto i) {
            return andwass::result<std::string, int>(andwass::ok(std::to_string(i)));
        });
        REQUIRE(res.is_ok());
        REQUIRE(*res.maybe_value() == 1234);
        REQUIRE(mapped.is_ok());
        REQUIRE(*mapped.maybe_value() == "1234");
    }

    {
        andwass::result<int, int> res = andwass::error(1234);
        auto mapped = res.and_then([](auto i) {
            return andwass::result<std::string, int>(andwass::ok(std::to_string(i)));
        });
        REQUIRE(res.is_error());
        REQUIRE(*res.maybe_error() == 1234);
        REQUIRE(mapped.is_error());
        REQUIRE(*mapped.maybe_error() == 1234);
    }

    //
    {
        andwass::result<void, int> res = andwass::ok();
        auto mapped =
            res.and_then([]() { return andwass::result<std::string, int>(andwass::ok("ok")); });
        REQUIRE(res.is_ok());
        REQUIRE(mapped.is_ok());
        REQUIRE(*mapped.maybe_value() == "ok");
    }

    {
        andwass::result<void, int> res = andwass::error(1234);
        auto mapped =
            res.and_then([]() { return andwass::result<std::string, int>(andwass::ok("ok")); });
        REQUIRE(res.is_error());
        REQUIRE(*res.maybe_error() == 1234);
        REQUIRE(mapped.is_error());
        REQUIRE(*mapped.maybe_error() == 1234);
    }

    //
    {
        andwass::result<int, int> res = andwass::ok(1234);
        auto mapped = res.and_then([](auto i) { return andwass::ok(std::to_string(i)); });
        REQUIRE(res.is_ok());
        REQUIRE(*res.maybe_value() == 1234);
        REQUIRE(mapped.is_ok());
        REQUIRE(*mapped.maybe_value() == "1234");
    }

    {
        andwass::result<int, int> res = andwass::error(1234);
        auto mapped = res.and_then([](auto i) { return andwass::ok(std::to_string(i)); });
        REQUIRE(res.is_error());
        REQUIRE(*res.maybe_error() == 1234);
        REQUIRE(mapped.is_error());
        REQUIRE(*mapped.maybe_error() == 1234);
    }

    //
    {
        andwass::result<void, int> res = andwass::ok();
        auto mapped = res.and_then([]() { return andwass::ok<std::string>("ok"); });
        REQUIRE(res.is_ok());
        REQUIRE(mapped.is_ok());
        REQUIRE(*mapped.maybe_value() == "ok");
    }

    {
        andwass::result<void, int> res = andwass::error(-1);
        auto mapped = res.and_then([]() { return andwass::ok<std::string>("ok"); });
        REQUIRE(res.is_error());
        REQUIRE(*res.maybe_error() == -1);
        REQUIRE(mapped.is_error());
        REQUIRE(*mapped.maybe_error() == -1);
    }

    //
    {
        andwass::result<int, int> res = andwass::ok(1);
        auto mapped = res.and_then([](auto i) { return andwass::error(-1); });
        REQUIRE(res.is_ok());
        REQUIRE(*res.maybe_value() == 1);
        REQUIRE(mapped.is_error());
        REQUIRE(*mapped.maybe_error() == -1);
    }

    {
        andwass::result<int, int> res = andwass::error(-1);
        auto mapped = res.and_then([](auto i) { return andwass::error(-2); });
        REQUIRE(res.is_error());
        REQUIRE(*res.maybe_error() == -1);
        REQUIRE(mapped.is_error());
        REQUIRE(*mapped.maybe_error() == -1);
    }

    //
    {
        andwass::result<void, int> res = andwass::ok();
        auto mapped = res.and_then([]() { return andwass::error(-1); });
        REQUIRE(res.is_ok());
        REQUIRE(mapped.is_error());
        REQUIRE(*mapped.maybe_error() == -1);
    }

    {
        andwass::result<void, int> res = andwass::error(-1);
        auto mapped = res.and_then([]() { return andwass::error(-2); });
        REQUIRE(res.is_error());
        REQUIRE(*res.maybe_error() == -1);
        REQUIRE(mapped.is_error());
        REQUIRE(*mapped.maybe_error() == -1);
    }

    //
    {
        const andwass::result<int, int> res = andwass::ok(1234);
        auto mapped = res.and_then([](auto i) {
            return andwass::result<std::string, int>(andwass::ok(std::to_string(i)));
        });
        REQUIRE(res.is_ok());
        REQUIRE(*res.maybe_value() == 1234);
        REQUIRE(mapped.is_ok());
        REQUIRE(*mapped.maybe_value() == "1234");
    }

    {
        andwass::result<int, int> res = andwass::ok(1234);
        auto mapped = std::move(res).and_then([](auto i) {
            return andwass::result<std::string, int>(andwass::ok(std::to_string(i)));
        });
        REQUIRE(mapped.is_ok());
        REQUIRE(*mapped.maybe_value() == "1234");
    }

    {
        const andwass::result<int, int> res = andwass::ok(1234);
        auto mapped = std::move(res).and_then([](auto i) {
            return andwass::result<std::string, int>(andwass::ok(std::to_string(i)));
        });
        REQUIRE(res.is_ok());
        REQUIRE(*res.maybe_value() == 1234);
        REQUIRE(mapped.is_ok());
        REQUIRE(*mapped.maybe_value() == "1234");
    }
}

TEST_CASE("basic-tests: or_else", "[basic-tests]") {
    //
    {
        andwass::result<void, int> res = andwass::ok();
        auto mapped = res.or_else(
            [](auto err) { return andwass::result<void, int>{andwass::error(2 * err)}; });
        REQUIRE(res.is_ok());
        REQUIRE(mapped.is_ok());
    }

    {
        andwass::result<void, int> res = andwass::error(-1);
        auto mapped =
            res.or_else([](auto i) { return andwass::result<void, int>{andwass::error(2 * i)}; });
        REQUIRE(res.is_error());
        REQUIRE(*res.maybe_error() == -1);
        REQUIRE(mapped.is_error());
        REQUIRE(*mapped.maybe_error() == -2);
    }

    //
    {
        andwass::result<int, int> res = andwass::ok(1);
        auto mapped = res.or_else([](auto err) { return andwass::ok(2); });
        REQUIRE(res.is_ok());
        REQUIRE(*res.maybe_value() == 1);
        REQUIRE(mapped.is_ok());
        REQUIRE(*mapped.maybe_value() == 1);
    }

    {
        andwass::result<int, int> res = andwass::error(-1);
        auto mapped = res.or_else([](auto i) { return andwass::ok(2 * i); });
        REQUIRE(res.is_error());
        REQUIRE(*res.maybe_error() == -1);
        REQUIRE(mapped.is_ok());
        REQUIRE(*mapped.maybe_value() == -2);
    }

    //
    {
        andwass::result<int, int> res = andwass::ok(1);
        auto mapped = res.or_else([](auto err) { return andwass::error(err * 2); });
        REQUIRE(res.is_ok());
        REQUIRE(*res.maybe_value() == 1);
        REQUIRE(mapped.is_ok());
        REQUIRE(*mapped.maybe_value() == 1);
    }

    {
        andwass::result<int, int> res = andwass::error(-1);
        auto mapped = res.or_else([](auto i) { return andwass::error(2 * i); });
        REQUIRE(res.is_error());
        REQUIRE(*res.maybe_error() == -1);
        REQUIRE(mapped.is_error());
        REQUIRE(*mapped.maybe_error() == -2);
    }

    //

    {
        const andwass::result<int, int> res = andwass::ok(1);
        auto mapped = res.or_else([](auto err) { return andwass::error(err * 2); });
        REQUIRE(res.is_ok());
        REQUIRE(*res.maybe_value() == 1);
        REQUIRE(mapped.is_ok());
        REQUIRE(*mapped.maybe_value() == 1);
    }

    {
        andwass::result<int, int> res = andwass::ok(1);
        auto mapped = std::move(res).or_else([](auto err) { return andwass::error(err * 2); });
        REQUIRE(mapped.is_ok());
        REQUIRE(*mapped.maybe_value() == 1);
    }

    {
        const andwass::result<int, int> res = andwass::ok(1);
        auto mapped = std::move(res).or_else([](auto err) { return andwass::error(err * 2); });
        REQUIRE(res.is_ok());
        REQUIRE(*res.maybe_value() == 1);
        REQUIRE(mapped.is_ok());
        REQUIRE(*mapped.maybe_value() == 1);
    }
}

TEST_CASE("basic-tests: unwrap_or_else", "[basic-tests]") {
    {
        andwass::result<int, int> res = andwass::ok(1);
        auto val = res.unwrap_or_else([](auto err) { return 2 * err; });

        REQUIRE(res.is_ok());
        REQUIRE(*res.maybe_value() == 1);
        REQUIRE(val == 1);
    }

    {
        andwass::result<int, int> res = andwass::error(-1);
        auto val = res.unwrap_or_else([](auto err) { return 2 * err; });

        REQUIRE(res.is_error());
        REQUIRE(*res.maybe_error() == -1);
        REQUIRE(val == -2);
    }

    //

    {
        andwass::result<void, int> res = andwass::ok();
        bool fb_called = false;
        res.unwrap_or_else([&](auto err) { fb_called = true; });

        REQUIRE(res.is_ok());
        REQUIRE_FALSE(fb_called);
    }

    {
        andwass::result<void, int> res = andwass::error(-1);
        bool fb_called = false;
        res.unwrap_or_else([&](auto err) { fb_called = true; });

        REQUIRE(res.is_error());
        REQUIRE(fb_called);
    }

    //

    {
        const andwass::result<int, int> res = andwass::ok(1);
        auto val = res.unwrap_or_else([](auto err) { return 2 * err; });

        REQUIRE(res.is_ok());
        REQUIRE(*res.maybe_value() == 1);
        REQUIRE(val == 1);
    }

    {
        andwass::result<int, int> res = andwass::ok(1);
        auto val = std::move(res).unwrap_or_else([](auto err) { return 2 * err; });

        REQUIRE(*res.maybe_value() == 1);
        REQUIRE(val == 1);
    }

    {
        const andwass::result<int, int> res = andwass::ok(1);
        auto val = std::move(res).unwrap_or_else([](auto err) { return 2 * err; });

        REQUIRE(res.is_ok());
        REQUIRE(*res.maybe_value() == 1);
        REQUIRE(val == 1);
    }
}

TEST_CASE("basic-tests: unwrap_or", "[basic-tests]") {
    {
        andwass::result<std::string, int> res = andwass::ok("hello");
        REQUIRE(res.unwrap_or("world") == "hello");
    }

    {
        andwass::result<std::string, int> res{andwass::error(-1)};
        REQUIRE(res.unwrap_or("world") == "world");
    }

    {
        andwass::result<std::string, int> res{andwass::ok("hello")};
        REQUIRE(std::move(res).unwrap_or("world") == "hello");
    }

    {
        andwass::result<std::string, int> res{andwass::error(-1)};
        REQUIRE(std::move(res).unwrap_or("world") == "world");
    }

    //

    {
        const andwass::result<std::string, int> res = andwass::ok("hello");
        REQUIRE(res.unwrap_or("world") == "hello");
    }

    {
        const andwass::result<std::string, int> res{andwass::error(-1)};
        REQUIRE(res.unwrap_or("world") == "world");
    }

    {
        const andwass::result<std::string, int> res{andwass::ok("hello")};
        REQUIRE(std::move(res).unwrap_or("world") == "hello");
    }

    {
        const andwass::result<std::string, int> res{andwass::error(-1)};
        REQUIRE(std::move(res).unwrap_or("world") == "world");
    }
}

TEST_CASE("basic-tests: unwrap", "[basic-tests]") {
    {
        andwass::result<std::string, int> res = andwass::ok("hello");
        REQUIRE(res.unwrap() == "hello");
    }

    {
        const andwass::result<std::string, int> res{andwass::ok("hello")};
        REQUIRE(res.unwrap() == "hello");
    }

    {
        andwass::result<std::string, int> res{andwass::ok("hello")};
        REQUIRE(std::move(res).unwrap() == "hello");
    }

    {
        const andwass::result<std::string, int> res{andwass::ok("hello")};
        REQUIRE(std::move(res).unwrap() == "hello");
    }
}

TEST_CASE("basic-tests: unwrap_error", "[basic-tests]") {
    {
        andwass::result<int, std::string> res{andwass::error("hello")};
        REQUIRE(res.unwrap_error() == "hello");
    }

    {
        const andwass::result<int, std::string> res{andwass::error("hello")};
        REQUIRE(res.unwrap_error() == "hello");
    }

    {
        andwass::result<int, std::string> res{andwass::error("hello")};
        REQUIRE(std::move(res).unwrap_error() == "hello");
    }

    {
        const andwass::result<int, std::string> res{andwass::error("hello")};
        REQUIRE(std::move(res).unwrap_error() == "hello");
    }
}

TEST_CASE("basic-tests: conversion copy", "[basic-tests]") {
    {
        const andwass::result<const char *, int> res{andwass::ok("ok")};
        andwass::result<std::string, std::int64_t> res2{res};
        REQUIRE(res.is_ok());
        REQUIRE(res2.is_ok());
        REQUIRE(res2.unwrap() == "ok");
    }

    {
        const andwass::result<int, const char *> res{andwass::error("error")};
        andwass::result<std::int64_t, std::string> res2{res};
        REQUIRE(res.is_error());
        REQUIRE(res2.is_error());
        REQUIRE(res2.unwrap_error() == "error");
    }

    //

    {
        const andwass::result<const char *, int> res{andwass::ok("ok")};
        andwass::result<std::string, std::int64_t> res2{andwass::ok("ok2")};
        REQUIRE(res.is_ok());
        REQUIRE(res2.is_ok());
        REQUIRE(res2.unwrap() == "ok2");

        res2 = res;
        REQUIRE(res2.is_ok());
        REQUIRE(res2.unwrap() == "ok");
    }

    {
        const andwass::result<int, const char *> res{andwass::error("error")};
        andwass::result<std::int64_t, std::string> res2{andwass::error("error2")};
        REQUIRE(res.is_error());
        REQUIRE(res2.is_error());
        REQUIRE(res2.unwrap_error() == "error2");

        res2 = res;
        REQUIRE(res2.is_error());
        REQUIRE(res2.unwrap_error() == "error");
    }
}

TEST_CASE("basic-tests: conversion move", "[basic-tests]") {
    {
        andwass::result<const char *, int> res{andwass::ok("ok")};
        andwass::result<std::string, std::int64_t> res2{std::move(res)};
        REQUIRE(res2.is_ok());
        REQUIRE(res2.unwrap() == "ok");
    }

    {
        andwass::result<int, const char *> res{andwass::error("error")};
        andwass::result<std::int64_t, std::string> res2{std::move(res)};
        REQUIRE(res2.is_error());
        REQUIRE(res2.unwrap_error() == "error");
    }

    //

    {
        andwass::result<const char *, int> res{andwass::ok("ok")};
        andwass::result<std::string, std::int64_t> res2{andwass::ok("ok2")};
        REQUIRE(res.is_ok());
        REQUIRE(res2.is_ok());
        REQUIRE(res2.unwrap() == "ok2");

        res2 = std::move(res);
        REQUIRE(res2.is_ok());
        REQUIRE(res2.unwrap() == "ok");
    }

    {
        andwass::result<int, const char *> res{andwass::error("error")};
        andwass::result<std::int64_t, std::string> res2{andwass::error("error2")};
        REQUIRE(res.is_error());
        REQUIRE(res2.is_error());
        REQUIRE(res2.unwrap_error() == "error2");

        res2 = std::move(res);
        REQUIRE(res2.is_error());
        REQUIRE(res2.unwrap_error() == "error");
    }
}

TEST_CASE("basic-tests: ok conversion assignment", "[basic-tests]") {
    //
    {
        const auto ok = andwass::ok("ok");
        andwass::result<std::string, std::int64_t> res2{ok};
        REQUIRE(res2.is_ok());
        REQUIRE(res2.unwrap() == "ok");
    }

    {
        const auto ok = andwass::ok("ok");
        andwass::result<std::string, std::int64_t> res2{andwass::ok("ok2")};
        REQUIRE(res2.is_ok());
        REQUIRE(res2.unwrap() == "ok2");

        res2 = ok;
        REQUIRE(res2.is_ok());
        REQUIRE(res2.unwrap() == "ok");
    }

    {
        auto ok = andwass::ok("ok");
        andwass::result<std::string, std::int64_t> res2{andwass::ok("ok2")};
        REQUIRE(res2.is_ok());
        REQUIRE(res2.unwrap() == "ok2");

        res2 = std::move(ok);
        REQUIRE(res2.is_ok());
        REQUIRE(res2.unwrap() == "ok");
    }

    {
        const auto ok = andwass::ok("ok");
        andwass::result<std::string, std::int64_t> res2{andwass::error(5)};
        REQUIRE_FALSE(res2.is_ok());
        REQUIRE(res2.unwrap_error() == 5);

        res2 = ok;
        REQUIRE(res2.is_ok());
        REQUIRE(res2.unwrap() == "ok");
    }

    {
        auto ok = andwass::ok("ok");
        andwass::result<std::string, std::int64_t> res2{andwass::error(5)};
        REQUIRE(res2.is_error());
        REQUIRE(res2.unwrap_error() == 5);

        res2 = std::move(ok);
        REQUIRE(res2.is_ok());
        REQUIRE(res2.unwrap() == "ok");
    }
}

TEST_CASE("basic-tests: error conversion assignment", "[basic-tests]") {
    //
    {
        const auto err = andwass::error("error");
        andwass::result<std::int64_t, std::string> res2{err};
        REQUIRE(res2.is_error());
        REQUIRE(res2.unwrap_error() == "error");
    }

    {
        const auto err = andwass::error("error");
        andwass::result<std::int64_t, std::string> res2{andwass::error("error2")};
        REQUIRE(res2.is_error());
        REQUIRE(res2.unwrap_error() == "error2");

        res2 = err;
        REQUIRE(res2.is_error());
        REQUIRE(res2.unwrap_error() == "error");
    }

    {
        auto err = andwass::error("error");
        andwass::result<std::int64_t, std::string> res2{andwass::error("error2")};
        REQUIRE(res2.is_error());
        REQUIRE(res2.unwrap_error() == "error2");

        res2 = std::move(err);
        REQUIRE(res2.is_error());
        REQUIRE(res2.unwrap_error() == "error");
    }

    {
        const auto err = andwass::error("error");
        andwass::result<std::int64_t, std::string> res2{andwass::ok(0x123456789ABCDEFLL)};
        REQUIRE(res2.is_ok());
        REQUIRE(res2.unwrap() == 0x123456789ABCDEFLL);

        res2 = err;
        REQUIRE(res2.is_error());
        REQUIRE(res2.unwrap_error() == "error");
    }

    {
        auto err = andwass::error("error");
        andwass::result<std::int64_t, std::string> res2{andwass::ok(0x123456789ABCDEFLL)};
        REQUIRE(res2.is_ok());
        REQUIRE(res2.unwrap() == 0x123456789ABCDEFLL);

        res2 = std::move(err);
        REQUIRE(res2.is_error());
        REQUIRE(res2.unwrap_error() == "error");
    }
}

TEST_CASE("basic-tests: equality comparison", "[basic-tests]") {
    {
        andwass::result<int, int> res1{andwass::ok(1)};
        andwass::result<int, int> res2{andwass::ok(1)};
        REQUIRE(res1 == res2);
        REQUIRE(res2 == res1);
        REQUIRE_FALSE(res1 != res2);
        REQUIRE_FALSE(res2 != res1);
    }
    {
        andwass::result<int, int> res1{andwass::ok(1)};
        andwass::result<int, int> res2{andwass::ok(2)};
        REQUIRE_FALSE(res1 == res2);
        REQUIRE_FALSE(res2 == res1);
        REQUIRE(res1 != res2);
        REQUIRE(res2 != res1);
    }
    {
        andwass::result<int, const char *> res1{andwass::ok(1)};
        andwass::result<short, std::string> res2{andwass::ok(1)};
        REQUIRE(res1 == res2);
        REQUIRE(res2 == res1);
        REQUIRE_FALSE(res1 != res2);
        REQUIRE_FALSE(res2 != res1);
    }
    {
        andwass::result<int, const char *> res1{andwass::ok(1)};
        andwass::result<short, std::string> res2{andwass::error("hello")};
        REQUIRE_FALSE(res1 == res2);
        REQUIRE_FALSE(res2 == res1);
        REQUIRE(res1 != res2);
        REQUIRE(res2 != res1);
    }
    {
        andwass::result<int, const char *> res1{andwass::error("hello")};
        andwass::result<short, std::string> res2{andwass::error("hello")};
        REQUIRE(res1 == res2);
        REQUIRE(res2 == res1);
        REQUIRE_FALSE(res1 != res2);
        REQUIRE_FALSE(res2 != res1);
    }
    {
        andwass::result<int, const char *> res1{andwass::error("world")};
        andwass::result<short, std::string> res2{andwass::error("hello")};
        REQUIRE_FALSE(res1 == res2);
        REQUIRE_FALSE(res2 == res1);
        REQUIRE(res1 != res2);
        REQUIRE(res2 != res1);
    }

    {
        andwass::result<void, const char *> res1{andwass::ok()};
        andwass::result<void, std::string> res2{andwass::ok()};
        REQUIRE(res1 == res2);
        REQUIRE(res2 == res1);
        REQUIRE_FALSE(res1 != res2);
        REQUIRE_FALSE(res2 != res1);
    }

    {
        andwass::result<void, const char *> res1{andwass::error("hello")};
        andwass::result<void, std::string> res2{andwass::ok()};
        REQUIRE_FALSE(res1 == res2);
        REQUIRE_FALSE(res2 == res1);
        REQUIRE(res1 != res2);
        REQUIRE(res2 != res1);
    }
    {
        andwass::result<void, const char *> res1{andwass::error("hello")};
        andwass::result<void, std::string> res2{andwass::error("hello")};
        REQUIRE(res1 == res2);
        REQUIRE(res2 == res1);
        REQUIRE_FALSE(res1 != res2);
        REQUIRE_FALSE(res2 != res1);
    }
    {
        andwass::result<void, const char *> res1{andwass::error("hello")};
        andwass::result<void, std::string> res2{andwass::error("world")};
        REQUIRE_FALSE(res1 == res2);
        REQUIRE_FALSE(res2 == res1);
        REQUIRE(res1 != res2);
        REQUIRE(res2 != res1);
    }
}

template <bool B>
struct NoexceptDflt
{
    NoexceptDflt() noexcept(B) {}
};
TEST_CASE("basic-tests: noexcept default propogation") {
    using NoexceptDfltFalse = NoexceptDflt<false>;
    using NoexceptDfltTrue = NoexceptDflt<true>;
    andwass::result<NoexceptDfltTrue, NoexceptDfltTrue> res1;
    andwass::result<NoexceptDfltFalse, NoexceptDfltTrue> res2;
    REQUIRE(std::is_nothrow_default_constructible_v<andwass::result<NoexceptDfltTrue, NoexceptDfltTrue>>);
    REQUIRE(std::is_nothrow_default_constructible_v<andwass::result<NoexceptDfltTrue, NoexceptDfltFalse>>);
    REQUIRE(!std::is_nothrow_default_constructible_v<andwass::result<NoexceptDfltFalse, NoexceptDfltTrue>>);
    REQUIRE(!std::is_nothrow_default_constructible_v<andwass::result<NoexceptDfltFalse, NoexceptDfltFalse>>);
}

template<bool B>
struct NoexceptMove
{
    NoexceptMove() = default;
    NoexceptMove(const NoexceptMove&) = default;
    NoexceptMove(NoexceptMove&&) noexcept(B) {}

    NoexceptMove& operator=(const NoexceptMove&) = default;
    NoexceptMove& operator=(NoexceptMove&&) noexcept(B) {}
};

TEST_CASE("basic-tests: noexcept move propogation") {
    using expected_noexcept = andwass::result<NoexceptMove<true>, NoexceptMove<true>>;
    using expected_throwable = andwass::result<NoexceptMove<false>, NoexceptMove<false>>;
    expected_noexcept res1{expected_noexcept{}};
    expected_throwable res2{expected_throwable{}};
    REQUIRE(std::is_nothrow_move_constructible_v<expected_noexcept>);
    REQUIRE(!std::is_nothrow_move_constructible_v<andwass::result<NoexceptMove<false>, NoexceptMove<true>>>);
    REQUIRE(!std::is_nothrow_move_constructible_v<andwass::result<NoexceptMove<true>, NoexceptMove<false>>>);
    REQUIRE(!std::is_nothrow_move_constructible_v<andwass::result<NoexceptMove<false>, NoexceptMove<false>>>);

    REQUIRE(std::is_nothrow_move_assignable_v<expected_noexcept>);
    REQUIRE(!std::is_nothrow_move_assignable_v<andwass::result<NoexceptMove<false>, NoexceptMove<true>>>);
    REQUIRE(!std::is_nothrow_move_assignable_v<andwass::result<NoexceptMove<true>, NoexceptMove<false>>>);
    REQUIRE(!std::is_nothrow_move_assignable_v<andwass::result<NoexceptMove<false>, NoexceptMove<false>>>);
}

template<bool B>
struct NoexceptCopy
{
    NoexceptCopy() = default;
    NoexceptCopy(const NoexceptCopy&) noexcept(B) {};
    NoexceptCopy(NoexceptCopy&&) = default;

    NoexceptCopy& operator=(const NoexceptCopy&) noexcept(B) {}
    NoexceptCopy& operator=(NoexceptCopy&&) = default;
};

TEST_CASE("basic-tests: noexcept copy propogation") {
    using expected_noexcept = andwass::result<NoexceptCopy<true>, NoexceptCopy<true>>;
    using expected_throwable = andwass::result<NoexceptCopy<false>, NoexceptCopy<false>>;
    expected_noexcept res1{expected_noexcept{}};
    expected_throwable res2{expected_throwable{}};
    REQUIRE(std::is_nothrow_copy_constructible_v<expected_noexcept>);
    REQUIRE(!std::is_nothrow_copy_constructible_v<andwass::result<NoexceptCopy<false>, NoexceptCopy<true>>>);
    REQUIRE(!std::is_nothrow_copy_constructible_v<andwass::result<NoexceptCopy<true>, NoexceptCopy<false>>>);
    REQUIRE(!std::is_nothrow_copy_constructible_v<andwass::result<NoexceptCopy<false>, NoexceptCopy<false>>>);

    REQUIRE(std::is_nothrow_copy_assignable_v<expected_noexcept>);
    REQUIRE(!std::is_nothrow_copy_assignable_v<andwass::result<NoexceptCopy<false>, NoexceptCopy<true>>>);
    REQUIRE(!std::is_nothrow_copy_assignable_v<andwass::result<NoexceptCopy<true>, NoexceptCopy<false>>>);
    REQUIRE(!std::is_nothrow_copy_assignable_v<andwass::result<NoexceptCopy<false>, NoexceptCopy<false>>>);
}

template<bool B>
struct NoexceptDestruct
{
    NoexceptDestruct() = default;
    ~NoexceptDestruct() noexcept(B) {}
    NoexceptDestruct(const NoexceptDestruct&) = default;
    NoexceptDestruct(NoexceptDestruct&&) = default;

    NoexceptDestruct& operator=(const NoexceptDestruct&) = default;
    NoexceptDestruct& operator=(NoexceptDestruct&&) = default;
};

TEST_CASE("basic-tests: noexcept destruct propogation") {
    using expected_noexcept = andwass::result<NoexceptDestruct<true>, NoexceptDestruct<true>>;
    using expected_throwable = andwass::result<NoexceptDestruct<false>, NoexceptDestruct<false>>;
    expected_noexcept res1{expected_noexcept{}};
    expected_throwable res2{expected_throwable{}};
    REQUIRE(std::is_nothrow_destructible_v<expected_noexcept>);
    REQUIRE(!std::is_nothrow_destructible_v<andwass::result<NoexceptDestruct<false>,NoexceptDestruct<true>>>);
    REQUIRE(!std::is_nothrow_destructible_v<andwass::result<NoexceptDestruct<true>, NoexceptDestruct<false>>>);
    REQUIRE(!std::is_nothrow_destructible_v<andwass::result<NoexceptDestruct<false>,NoexceptDestruct<false>>>);
}


#include <cstdio>

TEST_CASE("basic-tests: unwrap terminate", "[.terminating-unwrap]") {
    auto err = andwass::result<int, int>{andwass::error(1)};
    std::set_terminate([]() {
        puts("TERMINATING-UNWRAP: TERMINATE-CALLED\n");
        std::abort();
    });
    (void)err.unwrap();
}

TEST_CASE("basic-tests: unwrap void terminate", "[.terminating-void-unwrap]") {
    auto err = andwass::result<void, int>{andwass::error(1)};
    std::set_terminate([]() {
        puts("TERMINATING-VOID-UNWRAP: TERMINATE-CALLED\n");
        std::abort();
    });
    err.unwrap();
}

TEST_CASE("basic-tests: unwrap_error terminate", "[.terminating-unwrap-error]") {
    auto ok = andwass::result<int, int>{andwass::ok(1)};
    std::set_terminate([]() {
        puts("TERMINATING-UNWRAP-ERROR: TERMINATE-CALLED\n");
        std::abort();
    });
    (void)ok.unwrap_error();
}

#include <vector>
TEST_CASE("basic-tests: insert to vector") {
    std::vector<andwass::result<tracer, int>> v_tr;
    andwass::result<tracer, int> to_push{andwass::ok(tracer{})};
    tracer::reset();
    for (int i = 0; i < 1000000; i++) {
        v_tr.emplace_back(to_push);
    }

    printf("Copies: %d, moves: %d\n", tracer::copy_construct_count, tracer::move_construct_count);
}