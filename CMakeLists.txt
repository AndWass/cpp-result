cmake_minimum_required(VERSION 3.10)
project(cpp-result)

set(CXX_STANDARD_REQUIRED 17)
set(CMAKE_CXX_STANDARD 17)
add_library(andwass-result INTERFACE)
target_include_directories(andwass-result INTERFACE include)

target_sources(andwass-result INTERFACE
    ${CMAKE_CURRENT_LIST_DIR}/include/andwass/result.hpp
    ${CMAKE_CURRENT_LIST_DIR}/include/andwass/detail/result_storage.hpp
    ${CMAKE_CURRENT_LIST_DIR}/include/andwass/detail/invoke.hpp
    ${CMAKE_CURRENT_LIST_DIR}/include/andwass/detail/tag_type.hpp
    ${CMAKE_CURRENT_LIST_DIR}/include/andwass/detail/config.hpp
    )

add_library(andwass::result ALIAS andwass-result)

add_library(andwass-result-single-header INTERFACE)
target_include_directories(andwass-result-single-header INTERFACE single-header)
target_sources(andwass-result-single-header INTERFACE
    ${CMAKE_CURRENT_LIST_DIR}/single-header/andwass/result.hpp
)
add_library(andwass::result-single-header ALIAS andwass-result-single-header)

find_package(Python3 COMPONENTS Interpreter)

if(Python3_Interpreter_FOUND)
    add_custom_target(
        make-single-header
        ALL ${Python3_EXECUTABLE} -m quom include/andwass/result.hpp single-header/andwass/result.hpp
        BYPRODUCTS ${CMAKE_CURRENT_LIST_DIR}/single-header/andwass/result.hpp
        WORKING_DIRECTORY ${CMAKE_CURRENT_LIST_DIR}
    )
endif()
add_subdirectory(tests)
add_subdirectory(samples)
