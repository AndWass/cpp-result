
//          Copyright Andreas Wass 2019.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE or copy at
//          https://www.boost.org/LICENSE_1_0.txt)

/**
 * @file Contains a std::invoke replacement implemented in C++14
 */

#pragma once

#include <type_traits>
#include <utility>
#include <functional>

namespace andwass
{

namespace detail
{
template <class T>
struct is_reference_wrapper : std::false_type
{};
template <class U>
struct is_reference_wrapper<std::reference_wrapper<U>> : std::true_type
{};
template <class T>
constexpr bool is_reference_wrapper_v = is_reference_wrapper<T>::value;

struct tag_base_of
{
};
struct tag_ref_wrapper
{
};
struct tag_normal
{
};

template <bool MemberFn>
struct class_member_helper
{
    template <class T, class Type, class T1, class... Args>
    static decltype(auto) invoke(tag_base_of, Type T::*f, T1 &&t1, Args &&... args) {
        return (std::forward<T1>(t1).*f)(std::forward<Args>(args)...);
    }

    template <class T, class Type, class T1, class... Args>
    static decltype(auto) invoke(tag_ref_wrapper, Type T::*f, T1 &&t1, Args &&... args) {
        return (t1.get().*f)(std::forward<Args>(args)...);
    }

    template <class T, class Type, class T1, class... Args>
    static decltype(auto) invoke(tag_normal, Type T::*f, T1 &&t1, Args &&... args) {
        return ((*std::forward<T1>(t1)).*f)(std::forward<Args>(args)...);
    }
};

template <>
struct class_member_helper<false>
{
    template <class T, class Type, class T1, class... Args>
    static decltype(auto) invoke(tag_base_of, Type T::*f, T1 &&t1, Args &&... args) {
        static_assert(std::is_member_object_pointer<decltype(f)>::value);
        static_assert(sizeof...(args) == 0);
        return std::forward<T1>(t1).*f;
    }

    template <class T, class Type, class T1, class... Args>
    static decltype(auto) invoke(tag_ref_wrapper, Type T::*f, T1 &&t1, Args &&... args) {
        static_assert(std::is_member_object_pointer<decltype(f)>::value);
        static_assert(sizeof...(args) == 0);
        return t1.get().*f;
    }

    template <class T, class Type, class T1, class... Args>
    static decltype(auto) invoke(tag_normal, Type T::*f, T1 &&t1, Args &&... args) {
        static_assert(std::is_member_object_pointer<decltype(f)>::value);
        static_assert(sizeof...(args) == 0);
        return (*std::forward<T1>(t1)).*f;
    }
};

template <class T,
          class T1,
          bool BaseOf = std::is_base_of<T, std::decay_t<T1>>::value,
          bool RefWrapper = is_reference_wrapper_v<std::decay_t<T1>>>
struct member_fn_tag {};

template <class T, class T1>
struct member_fn_tag<T, T1, true, false>
{ using type = tag_base_of; };

template <class T, class T1>
struct member_fn_tag<T, T1, false, true>
{ using type = tag_ref_wrapper; };

template <class T, class T1>
struct member_fn_tag<T, T1, false, false>
{ using type = tag_normal; };

template <class T, class Type, class T1, class... Args>
decltype(auto) INVOKE(Type T::*f, T1 &&t1, Args &&... args) {
    using tag_type = typename member_fn_tag<T, T1>::type;
    return class_member_helper<std::is_member_function_pointer<decltype(f)>::value>::invoke(
        tag_type{}, f, std::forward<T1>(t1), std::forward<Args>(args)...);
}

template <class F, class... Args>
decltype(auto) INVOKE(F &&f, Args &&... args) {
    return std::forward<F>(f)(std::forward<Args>(args)...);
}
} // namespace detail

template <class F, class... Args>
auto invoke(F &&f, Args &&... args) {
    return detail::INVOKE(std::forward<F>(f), std::forward<Args>(args)...);
}
} // namespace andwass