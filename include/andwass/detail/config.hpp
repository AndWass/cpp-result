

//          Copyright Andreas Wass 2019.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE or copy at
//          https://www.boost.org/LICENSE_1_0.txt)

#pragma once

#if defined(__cpp_lib_launder)
#define LAUNDER(x) std::launder(x)
#else
#define LAUNDER(x) x
#endif

#if defined(__has_cpp_attribute)
#if __has_cpp_attribute(nodiscard) >= 201603
#define ANDWASS_USE_NODISCARD
#endif
#endif

#ifdef ANDWASS_USE_NODISCARD
#define NODISCARD [[nodiscard]]
#else
#define NODISCARD
#endif

#ifdef _MSVC_LANG
#define ANDWASS_RESULT_CPP_VER _MSVC_LANG
#else
#define ANDWASS_RESULT_CPP_VER __cplusplus
#endif

#if !defined(ANDWASS_RESULT_CPP_VER) || ANDWASS_RESULT_CPP_VER < 201402L
#error "Must build with C++ 14 or higher, use c++17 or higher for best result"
#endif

#if ANDWASS_RESULT_CPP_VER >= 201703L
#define ANDWASS_RESULT_USE_OPTIONAL

namespace andwass
{
namespace result_helper
{
template <class... Ts>
constexpr decltype(auto) invoke(Ts&&... ts) {
    return std::invoke(std::forward<Ts>(ts)...);
}

template <class Fn, class... Ts>
using invoke_result_t = std::invoke_result_t<Fn, Ts...>;

} // namespace result_helper
} // namespace andwass

#else
#define ANDWASS_RESULT_USE_EXPERIMENTAL_OPTIONAL

#include "invoke.hpp"

namespace andwass
{
namespace result_helper
{
template <class F, class... Ts>
constexpr auto invoke(F&& f, Ts&&... ts) {
    return andwass::invoke(std::forward<F>(f), std::forward<Ts>(ts)...);
}

template <class F, class... Ts>
using invoke_result_t = typename std::result_of<F(Ts...)>::type;

} // namespace result_helper
} // namespace andwass
#endif

#if defined(ANDWASS_RESULT_NO_OPTIONAL)
#elif defined ANDWASS_RESULT_USE_BOOST_OPTIONAL
#include <boost/optional.hpp>
namespace andwass
{
template <class T>
using optional_type = boost::optional<T>;
static inline auto nullopt = boost::none;
} // namespace andwass
#elif defined(ANDWASS_RESULT_USE_OPTIONAL)
#include <optional>
namespace andwass
{
template <class T>
using optional_type = std::optional<T>;
constexpr auto nullopt = std::nullopt;
} // namespace andwass
#else
#include <experimental/optional>
namespace andwass
{
template <class T>
using optional_type = std::experimental::optional<T>;
constexpr auto nullopt = std::experimental::nullopt;
} // namespace andwass
#endif