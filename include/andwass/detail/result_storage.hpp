
//          Copyright Andreas Wass 2019.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE or copy at
//          https://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <cstdint>
#include <type_traits>
#include <utility>

namespace andwass
{
namespace result_detail
{
struct no_init_tag
{
};

struct inplace_ok_tag
{
};
struct inplace_error_tag
{
};

template <class... T>
constexpr bool default_constructible_union_v = (std::is_default_constructible<T>::value && ...);

template <class... T>
constexpr bool trivially_destructible_union_v = (std::is_trivially_destructible<T>::value && ...);

template <class... T>
constexpr bool move_constructible_union_v = (std::is_move_constructible<T>::value && ...);

template <class... T>
constexpr bool nothrow_move_constructible_union_v = (std::is_nothrow_move_constructible<T>::value &&
                                                     ...);

template <class... T>
constexpr bool
    trivially_move_constructible_union_v = (std::is_trivially_move_constructible<T>::value && ...);

template <class... T>
constexpr bool copy_constructible_union_v = (std::is_copy_constructible<T>::value && ...);
template <class... T>
constexpr bool nothrow_copy_constructible_union_v = (std::is_nothrow_copy_constructible<T>::value &&
                                                     ...);

template <class... T>
constexpr bool
    trivially_copy_constructible_union_v = (std::is_trivially_copy_constructible<T>::value && ...);

template <class... T>
constexpr bool move_assignable_union_v = (std::is_move_assignable<T>::value && ...);

template <class... T>
constexpr bool nothrow_move_assignable_union_v = (std::is_nothrow_move_assignable<T>::value && ...);

template <class... T>
constexpr bool trivially_move_assignable_union_v = (std::is_trivially_move_assignable<T>::value &&
                                                    ...);

template <class... T>
constexpr bool copy_assignable_union_v = (std::is_copy_assignable<T>::value && ...);

template <class... T>
constexpr bool nothrow_copy_assignable_union_v = (std::is_nothrow_copy_assignable<T>::value && ...);

template <class... T>
constexpr bool trivially_copy_assignable_union_v = (std::is_trivially_copy_assignable<T>::value &&
                                                    ...);

template <bool TrivialDestruct>
struct storage_base_tag
{};

template <class T, class E>
using storage_base_tag_for = storage_base_tag<trivially_destructible_union_v<T, E>>;

template <bool MoveConstructible, bool Trivial>
struct move_tag
{};

template <class... T>
using move_tag_for =
    move_tag<move_constructible_union_v<T...>, trivially_move_constructible_union_v<T...>>;

template <bool CopyConstructible, bool Trivial>
struct copy_tag
{};

template <class... T>
using copy_tag_for =
    copy_tag<copy_constructible_union_v<T...>, trivially_copy_constructible_union_v<T...>>;

template <class T, class E, bool TrivialDestructor = trivially_destructible_union_v<T, E>>
struct result_storage_base;

template <class T, class E>
struct result_storage_base<T, E, true>
{
    union
    {
        T val_;
        E err_;
        char no_init_;
    };
    bool has_val_ = true;

    constexpr result_storage_base() noexcept(std::is_nothrow_default_constructible<T>::value)
        : val_{}, has_val_{true} {
    }

    constexpr result_storage_base(no_init_tag) noexcept : no_init_{} {
    }

    template <class... Args>
    constexpr result_storage_base(inplace_ok_tag, Args&&... args) noexcept(
        std::is_nothrow_constructible<T, Args...>::value)
        : val_{std::forward<Args>(args)...}, has_val_{true} {
    }

    template <class... Args>
    constexpr result_storage_base(inplace_error_tag, Args&&... args) noexcept(
        std::is_nothrow_constructible<E, Args...>::value)
        : err_{std::forward<Args>(args)...}, has_val_{false} {
    }

    void destroy_current() noexcept {
    }
};

template <class T, class E>
struct result_storage_base<T, E, false>
{
    union
    {
        T val_;
        E err_;
        char dummy_;
    };
    bool has_val_ = true;

    constexpr result_storage_base() noexcept(std::is_nothrow_default_constructible<T>::value)
        : val_{} {
    }
    constexpr result_storage_base(no_init_tag) noexcept : dummy_{} {
    }

    template <class... Args>
    constexpr result_storage_base(inplace_ok_tag, Args&&... args) noexcept(
        std::is_nothrow_constructible<T, Args...>::value)
        : val_{std::forward<Args>(args)...}, has_val_{true} {
    }

    template <class... Args>
    constexpr result_storage_base(inplace_error_tag, Args&&... args) noexcept(
        std::is_nothrow_constructible<E, Args...>::value)
        : err_{std::forward<Args>(args)...}, has_val_{false} {
    }

    ~result_storage_base() noexcept(noexcept(this->destroy_current())) {
        destroy_current();
    }

    void destroy_current() noexcept(std::is_nothrow_destructible<T>::value &&
                                    std::is_nothrow_destructible<E>::value) {
        if (has_val_) {
            val_.~T();
        }
        else {
            err_.~E();
        }
    }
};

template<class T, class E, bool DefaultConstructible = default_constructible_union_v<T, E>>
struct result_storage_default_constructor_base: result_storage_base<T, E>
{
    using base_ = result_storage_base<T, E>;
    using base_::base_;
    using base_::operator=;
};

template<class T, class E>
struct result_storage_default_constructor_base<T, E, false>: result_storage_base<T, E>
{
    using base_ = result_storage_base<T, E>;
    using base_::base_;
    using base_::operator=;

    result_storage_default_constructor_base() = delete;
};

template <class T, class E>
struct result_storage_operations_base : result_storage_default_constructor_base<T, E>
{
    using base_ = result_storage_default_constructor_base<T, E>;
    using base_::base_;
    using base_::operator=;

    void copy_construct(const T& t) noexcept(std::is_nothrow_copy_constructible<T>::value) {
        ::new (std::addressof(base_::val_)) T(t);
    }
    void copy_construct(const E& e) noexcept(std::is_nothrow_copy_constructible<E>::value) {
        ::new (std::addressof(base_::err_)) E(e);
    }
    void move_construct(T&& t) noexcept(std::is_nothrow_move_constructible<T>::value) {
        ::new (std::addressof(base_::val_)) T(std::move(t));
    }
    void move_construct(E&& e) noexcept(std::is_nothrow_move_constructible<E>::value) {
        ::new (std::addressof(base_::err_)) E(std::move(e));
    }
};

template <class T,
          class E,
          bool MoveAssignable = move_assignable_union_v<T, E>,
          bool Trivial = trivially_move_assignable_union_v<T, E>,
          bool MoveConstructible = move_constructible_union_v<T, E>>
struct move_assign_result_storage_base : result_storage_operations_base<T, E>
{
    // This is the case of trivially move assignable
    using base_ = result_storage_operations_base<T, E>;
    using base_::base_;
    using base_::operator=;
};

template <class T, class E>
struct move_assign_result_storage_base<T, E, false, false, false>
    : result_storage_operations_base<T, E>
{
    using base_ = result_storage_operations_base<T, E>;
    using base_::base_;
    using base_::operator=;

    move_assign_result_storage_base(const move_assign_result_storage_base&) = default;
    move_assign_result_storage_base(move_assign_result_storage_base&&) = default;

    move_assign_result_storage_base& operator=(const move_assign_result_storage_base&) = default;
    move_assign_result_storage_base& operator=(move_assign_result_storage_base&&) = delete;
};

template <class T, class E>
struct move_assign_result_storage_base<T, E, false, false, true>
    : result_storage_operations_base<T, E>
{
    using base_ = result_storage_operations_base<T, E>;
    using base_::base_;
    using base_::operator=;

    move_assign_result_storage_base(const move_assign_result_storage_base&) = default;
    move_assign_result_storage_base(move_assign_result_storage_base&&) = default;

    move_assign_result_storage_base& operator=(const move_assign_result_storage_base&) = default;
    move_assign_result_storage_base& operator=(move_assign_result_storage_base&& rhs) noexcept(
        noexcept(this->destroy_current()) &&
        noexcept(this->move_construct(std::declval<T>()) &&
                 noexcept(this->move_construct(std::declval<E>())))) {
        if (this == &rhs) {
            return *this;
        }
        base_::destroy_current();
        base_::has_val_ = rhs.has_val_;
        if (rhs.has_val_) {
            base_::move_construct(std::move(rhs.val_));
        }
        else {
            base_::move_construct(std::move(rhs.err_));
        }
        return *this;
    }
};

template <class T, class E>
struct move_assign_result_storage_base<T, E, true, false, true>
    : result_storage_operations_base<T, E>
{
    using base_ = result_storage_operations_base<T, E>;
    using base_::base_;
    using base_::operator=;

    move_assign_result_storage_base(const move_assign_result_storage_base&) = default;
    move_assign_result_storage_base(move_assign_result_storage_base&&) = default;

    move_assign_result_storage_base& operator=(const move_assign_result_storage_base&) = default;
    move_assign_result_storage_base& operator=(move_assign_result_storage_base&& rhs) noexcept(
        noexcept(this->destroy_current()) && nothrow_move_assignable_union_v<T, E> &&
        noexcept(this->move_construct(std::declval<T>())) &&
        noexcept(this->move_construct(std::declval<E>()))) {
        if (this == &rhs) {
            return *this;
        }
        if (rhs.has_val_ == base_::has_val_) {
            if (rhs.has_val_) {
                base_::val_ = std::move(rhs.val_);
            }
            else {
                base_::err_ = std::move(rhs.err_);
            }
        }
        else {
            base_::destroy_current();
            base_::has_val_ = rhs.has_val_;
            if (rhs.has_val_) {
                base_::move_construct(std::move(rhs.val_));
            }
            else {
                base_::move_construct(std::move(rhs.err_));
            }
        }
        return *this;
    }
};

template <class T,
          class E,
          bool MoveConstructible = move_constructible_union_v<T, E>,
          bool Trivial = trivially_move_constructible_union_v<T, E>>
struct move_result_storage_base;

template <class T, class E>
struct move_result_storage_base<T, E, true, false> : move_assign_result_storage_base<T, E>
{
    using base_ = move_assign_result_storage_base<T, E>;
    using base_::base_;
    using base_::operator=;
    move_result_storage_base(move_result_storage_base&& rhs) noexcept(
        std::is_nothrow_move_constructible<T>::value &&
        std::is_nothrow_move_constructible<E>::value)
        : base_{no_init_tag{}} {
        base_::has_val_ = rhs.has_val_;
        if (rhs.has_val_) {
            ::new (std::addressof(base_::val_)) T{std::move(rhs.val_)};
        }
        else {
            ::new (std::addressof(base_::err_)) E{std::move(rhs.err_)};
        }
    }
    move_result_storage_base(const move_result_storage_base&) = default;

    move_result_storage_base& operator=(move_result_storage_base&& rhs) = default;
    move_result_storage_base& operator=(const move_result_storage_base&) = default;
};

template <class T, class E>
struct move_result_storage_base<T, E, false, false> : move_assign_result_storage_base<T, E>
{
    using base_ = move_assign_result_storage_base<T, E>;
    using base_::base_;
    using base_::operator=;
    move_result_storage_base(move_result_storage_base&&) = delete;
    move_result_storage_base(const move_result_storage_base&) = default;

    move_result_storage_base& operator=(move_result_storage_base&&) = delete;
    move_result_storage_base& operator=(const move_result_storage_base&) = default;
};

template <class T, class E>
struct move_result_storage_base<T, E, true, true> : move_assign_result_storage_base<T, E>
{
    using base_ = move_assign_result_storage_base<T, E>;
    using base_::base_;
    using base_::operator=;
    move_result_storage_base(move_result_storage_base&&) = default;
    move_result_storage_base(const move_result_storage_base&) = default;

    move_result_storage_base& operator=(move_result_storage_base&&) = default;
    move_result_storage_base& operator=(const move_result_storage_base&) = default;
};

template <class T,
          class E,
          bool CopyAssignable = copy_assignable_union_v<T, E>,
          bool Trivial = trivially_copy_assignable_union_v<T, E>,
          bool CopyConstructible = copy_constructible_union_v<T, E>>
struct copy_assign_result_storage_base : move_result_storage_base<T, E>
{
    // This is the case of trivially copy assignable
    using base_ = move_result_storage_base<T, E>;
    using base_::base_;
    using base_::operator=;
};

template <class T, class E>
struct copy_assign_result_storage_base<T, E, false, false, false> : move_result_storage_base<T, E>
{
    using base_ = move_result_storage_base<T, E>;
    using base_::base_;
    using base_::operator=;

    copy_assign_result_storage_base(const copy_assign_result_storage_base&) = default;
    copy_assign_result_storage_base(copy_assign_result_storage_base&&) = default;

    copy_assign_result_storage_base& operator=(const copy_assign_result_storage_base&) = delete;
    copy_assign_result_storage_base& operator=(copy_assign_result_storage_base&&) = default;
};

template <class T, class E>
struct copy_assign_result_storage_base<T, E, false, false, true> : move_result_storage_base<T, E>
{
    using base_ = move_result_storage_base<T, E>;
    using base_::base_;
    using base_::operator=;

    copy_assign_result_storage_base(const copy_assign_result_storage_base&) = default;
    copy_assign_result_storage_base(copy_assign_result_storage_base&&) = default;

    copy_assign_result_storage_base& operator=(copy_assign_result_storage_base&&) = default;
    copy_assign_result_storage_base& operator=(const copy_assign_result_storage_base& rhs) noexcept(
        noexcept(this->destroy_current()) && noexcept(this->copy_construct(std::declval<T>())) &&
        noexcept(this->copy_construct(std::declval<E>()))) {
        if (this == &rhs) {
            return *this;
        }
        base_::destroy_current();
        base_::has_val_ = rhs.has_val_;
        if (rhs.has_val_) {
            base_::copy_construct(rhs.val_);
        }
        else {
            base_::copy_construct(rhs.err_);
        }
        return *this;
    }
};

template <class T, class E>
struct copy_assign_result_storage_base<T, E, true, false, true> : move_result_storage_base<T, E>
{
    using base_ = move_result_storage_base<T, E>;
    using base_::base_;
    using base_::operator=;

    copy_assign_result_storage_base(const copy_assign_result_storage_base&) = default;
    copy_assign_result_storage_base(copy_assign_result_storage_base&&) = default;

    copy_assign_result_storage_base& operator=(copy_assign_result_storage_base&&) = default;
    copy_assign_result_storage_base& operator=(const copy_assign_result_storage_base& rhs) noexcept(
        noexcept(this->destroy_current()) && nothrow_copy_constructible_union_v<T, E> &&
        nothrow_copy_constructible_union_v<T, E>) {
        if (this == &rhs) {
            return *this;
        }
        if (rhs.has_val_ == base_::has_val_) {
            if (rhs.has_val_) {
                base_::val_ = rhs.val_;
            }
            else {
                base_::err_ = rhs.err_;
            }
        }
        else {
            base_::destroy_current();
            base_::has_val_ = rhs.has_val_;
            if (rhs.has_val_) {
                this->copy_construct(rhs.val_);
            }
            else {
                this->copy_construct(rhs.err_);
            }
        }
        return *this;
    }
};

template <class T,
          class E,
          bool Copyable = copy_constructible_union_v<T, E>,
          bool Trivial = trivially_copy_constructible_union_v<T, E>>
struct copy_result_storage_base;

template <class T, class E>
struct copy_result_storage_base<T, E, true, false> : copy_assign_result_storage_base<T, E>
{
    using base_ = copy_assign_result_storage_base<T, E>;
    using base_::base_;
    using base_::operator=;
    copy_result_storage_base(const copy_result_storage_base& rhs) noexcept(
        nothrow_copy_constructible_union_v<T, E>)
        : base_{no_init_tag{}} {
        base_::has_val_ = rhs.has_val_;
        if (rhs.has_val_) {
            ::new (std::addressof(base_::val_)) T{rhs.val_};
        }
        else {
            ::new (std::addressof(base_::err_)) E{rhs.err_};
        }
    }
    copy_result_storage_base(copy_result_storage_base&&) = default;

    copy_result_storage_base& operator=(const copy_result_storage_base& rhs) = default;
    copy_result_storage_base& operator=(copy_result_storage_base&&) = default;
};

template <class T, class E>
struct copy_result_storage_base<T, E, false, false> : copy_assign_result_storage_base<T, E>
{
    using base_ = copy_assign_result_storage_base<T, E>;
    using base_::base_;
    using base_::operator=;
    copy_result_storage_base(const copy_result_storage_base&) = delete;
    copy_result_storage_base(copy_result_storage_base&&) = default;

    copy_result_storage_base& operator=(const copy_result_storage_base&) = delete;
    copy_result_storage_base& operator=(copy_result_storage_base&&) = default;
};

template <class T, class E>
struct copy_result_storage_base<T, E, true, true> : copy_assign_result_storage_base<T, E>
{
    using base_ = copy_assign_result_storage_base<T, E>;
    using base_::base_;
    using base_::operator=;
    copy_result_storage_base(const copy_result_storage_base&) = default;
    copy_result_storage_base(copy_result_storage_base&&) = default;

    copy_result_storage_base& operator=(const copy_result_storage_base&) = default;
    copy_result_storage_base& operator=(copy_result_storage_base&&) = default;
};

template <class T, class E>
struct result_storage : copy_result_storage_base<T, E>
{
    using base_ = copy_result_storage_base<T, E>;
    using base_::base_;
    using base_::operator=;

    using value_type = T;
    using error_type = E;

    void destroy_current() noexcept(
        noexcept(this->base_::destroy_current())
    ) {
        base_::destroy_current();
    }

    T* value_ptr() noexcept {
        return LAUNDER(std::addressof(base_::val_));
    }

    const T* value_ptr() const noexcept {
        return LAUNDER(std::addressof(base_::val_));
    }

    E* error_ptr() noexcept {
        return LAUNDER(std::addressof(base_::err_));
    }

    const E* error_ptr() const noexcept {
        return LAUNDER(std::addressof(base_::err_));
    }

    NODISCARD constexpr bool has_val() const {
        return base_::has_val_;
    }
};
} // namespace result_detail
} // namespace andwass