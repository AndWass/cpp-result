
//          Copyright Andreas Wass 2019.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE or copy at
//          https://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <type_traits>

namespace andwass
{
namespace result_detail
{
struct monostate
{
};

template <class TypeT, bool DefaultConstructible = std::is_default_constructible<TypeT>::value>
struct default_constructible_base
{};

template <class TypeT>
struct default_constructible_base<TypeT, false>
{
    default_constructible_base() = delete;
    default_constructible_base(const default_constructible_base&) = default;
    default_constructible_base(default_constructible_base&&) noexcept = default;

    default_constructible_base& operator=(const default_constructible_base&) = default;
    default_constructible_base& operator=(default_constructible_base&&) noexcept = default;
};

template <class TypeT, bool Constructible = std::is_copy_constructible<TypeT>::value>
struct copy_constructible_base
{};

template <class TypeT>
struct copy_constructible_base<TypeT, false>
{
    copy_constructible_base() = default;
    copy_constructible_base(const copy_constructible_base&) = delete;
    copy_constructible_base(copy_constructible_base&&) noexcept = default;

    copy_constructible_base& operator=(const copy_constructible_base&) = default;
    copy_constructible_base& operator=(copy_constructible_base&&) noexcept = default;
};

template <class TypeT, bool Constructible = std::is_move_constructible<TypeT>::value>
struct move_constructible_base
{};

template <class TypeT>
struct move_constructible_base<TypeT, false>
{
    move_constructible_base() = default;
    move_constructible_base(const move_constructible_base&) = default;
    move_constructible_base(move_constructible_base&&) noexcept = delete;

    move_constructible_base& operator=(const move_constructible_base&) = default;
    move_constructible_base& operator=(move_constructible_base&&) noexcept = default;
};

template <class TypeT>
struct ok_tag : default_constructible_base<TypeT>,
                copy_constructible_base<TypeT>,
                move_constructible_base<TypeT>
{
    using default_constructible_base<TypeT>::default_constructible_base;
    using copy_constructible_base<TypeT>::copy_constructible_base;
    using move_constructible_base<TypeT>::move_constructible_base;

    using value_type = TypeT;
    using reference = TypeT&;
    using const_reference = const TypeT&;
    using rvalue_reference = TypeT&&;
    value_type value;

    ok_tag() = default;
    ok_tag(const ok_tag&) = default;
    ok_tag(ok_tag&&) = default;

    ok_tag& operator=(const ok_tag&) = default;
    ok_tag& operator=(ok_tag&&) = default;
    ~ok_tag() = default;

    template <class V,
              std::enable_if_t<std::is_constructible<value_type, V>::value &&
                               !std::is_same<std::decay_t<V>, ok_tag<TypeT>>::value>* = nullptr>
    ok_tag(V&& v) noexcept : value(std::forward<V>(v)) {
    }
};

template <>
struct ok_tag<void>
{
    using value_type = monostate;
    using reference = monostate;
    using const_reference = monostate;
    using rvalue_reference = monostate;
    value_type value;
};

template <class T>
struct ok_traits;

template <class T>
struct ok_traits<ok_tag<T>>
{ using type = T; };

template <class T>
struct is_ok_tag : public std::false_type
{};

template <class T>
struct is_ok_tag<ok_tag<T>> : public std::true_type
{};

template <class TypeT>
struct error_tag
{
    using value_type = TypeT;
    using reference = TypeT&;
    using const_reference = const TypeT&;
    value_type value;

    error_tag() = default;
    template <class V, std::enable_if_t<std::is_constructible<value_type, V>::value>* = nullptr>
    error_tag(V&& v) noexcept : value(std::forward<V>(v)) {
    }
};

template <class T>
struct is_error_tag : public std::false_type
{};

template <class T>
struct is_error_tag<error_tag<T>> : public std::true_type
{};

template <class T>
struct error_traits;

template <class T>
struct error_traits<error_tag<T>>
{ using type = T; };

} // namespace result_detail
} // namespace andwass